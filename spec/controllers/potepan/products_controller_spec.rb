require "rails_helper"

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "showページ" do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
    let!(:not_related_product) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it "200レスポンスを返す" do
      expect(response).to have_http_status "200"
    end

    it "showテンプレートを返す" do
      expect(response).to render_template :show
    end

    it 'インスタンス変数@productが存在する' do
      expect(assigns(:product)).to eq product
    end

    it "4つの関連商品が存在する" do
      expect(assigns(:related_products).count).to eq 4
    end

    it "関連した商品のみ存在する" do
      expect(assigns(:related_products)).not_to include not_related_product
    end
  end
end
