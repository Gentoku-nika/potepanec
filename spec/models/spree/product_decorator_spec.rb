require "rails_helper"

RSpec.describe Spree::ProductDecorator, type: :model do
  let!(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  it "related_productは関連商品を含める" do
    expect(product.related_products).to include related_product
  end

  it "関連商品にレシーバーが含まれない" do
    expect(product.related_products).not_to include product
  end
end
