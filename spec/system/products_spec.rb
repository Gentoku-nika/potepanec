require 'rails_helper'

RSpec.describe 'Products', type: :system do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
  let!(:not_related_product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  context "商品詳細の表示" do
    it "カテゴリーページに戻るリンクがあり、商品のカテゴリーアイテムが表示される" do
      within ".media-body" do
        click_on "一覧ページへ戻る"
        expect(page).to have_current_path(potepan_category_path(taxon.id))
      end
    end

    it "「商品名」「値段」「画像」「説明文」が表示されている" do
      within ".media-body" do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
        expect(page).to have_content product.display_image.attachment_file_name
        expect(page).to have_content product.description
      end
    end
  end

  context "関連商品の表示" do
    it "関連した「商品名」「値段」「画像」が表示されている" do
      within ".productsContent" do
        expect(page).to have_content related_products[0].name
        expect(page).to have_content related_products[0].display_price
        expect(page).to have_content related_products[0].display_image.attachment_file_name
      end
    end

    it "4つの関連商品が表示される" do
      within ".productsContent" do
        expect(page).to have_selector ".productBox", count: 4
      end
    end

    it "関連商品をクリック時、商品詳細ページが表示されている" do
      within ".productsContent" do
        click_on related_products[0].name
        expect(page).to have_current_path(potepan_product_path(related_products[0].id))
      end
    end

    it "関連商品には@productの商品は重複して表示されない" do
      within ".productsContent" do
        expect(page).not_to have_content product.name
      end
    end

    it "関連しない商品は表示しない" do
      within ".productsContent" do
        expect(page).not_to have_content not_related_product.name
      end
    end
  end
end
