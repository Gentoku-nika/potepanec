require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "showページ" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "正常なレスポンスを返す" do
      expect(response).to have_http_status :success
    end

    it "showテンプレートを返す" do
      expect(response).to render_template :show
    end

    context "インスタンス変数の存在" do
      it "@taxonが存在する" do
        expect(assigns(:taxon)).to eq taxon
      end

      it "@taxonomyが存在する" do
        expect(assigns(:taxonomies)).to include taxonomy
      end

      it "@productsが存在する" do
        expect(assigns(:products)).to include product
      end
    end
  end
end
