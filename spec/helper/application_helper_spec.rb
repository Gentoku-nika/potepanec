require "rails_helper"

RSpec.describe ApplicationHelper, type: :helper do
  describe "solidusタイトルテスト" do
    include ApplicationHelper
    subject { full_title(page_title) }

    context "タイトルに「空文字」が渡された時" do
      let(:page_title) { "" }

      it "「BIGBAG Store」で表示" do
        is_expected.to eq "BIGBAG Store"
      end
    end

    context "page_titleにnilが渡された場合" do
      let(:page_title) { nil }

      it "「BIGBAG Store」と表示" do
        is_expected.to eq("BIGBAG Store")
      end
    end

    context "タイトルにアイテム名が渡された時" do
      let(:page_title) { "Ruby on Rails Baseball" }

      it "Ruby on Rails Baseball - BIGBAG Storeで表示" do
        is_expected.to eq "Ruby on Rails Baseball - BIGBAG Store"
      end
    end
  end
end
