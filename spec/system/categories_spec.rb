require 'rails_helper'

RSpec.describe 'Categories', type: :system do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon_1) { create(:taxon, name: "T-Shirts", taxonomy: taxonomy, parent: taxonomy.root) }
  let(:taxon_2) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:product_1) { create(:product, name: "rails-T", taxons: [taxon_1]) }
  let!(:product_2) { create(:product, name: "ruby-bag", taxons: [taxon_2]) }

  before do
    visit potepan_category_path(taxon_1.id)
  end

  context "商品カテゴリーの表示" do
    it "taxonomyが表示されている" do
      within ".side-nav" do
        expect(page).to have_content taxonomy.name
      end
    end

    it "taxonが表示されている" do
      within ".side-nav" do
        expect(page).to have_content taxon_1.name
      end
    end

    it "taxonカウントが表示されている" do
      within ".side-nav" do
        expect(page).to have_content "(#{taxon_1.all_products.count})"
      end
    end
  end

  context "商品が表示されている" do
    it "商品名が表示されている" do
      within ".productBox" do
        expect(page).to have_content product_1.name
      end
    end

    it "価格が表示されている" do
      within ".productBox" do
        expect(page).to have_content product_1.display_price
      end
    end
  end

  context "カテゴリーページから商品ページにアクセスした時" do
    it "商品ページに飛べる" do
      click_on product_1.name
      expect(page).to have_current_path potepan_product_path(product_1.id)
    end

    it "商品名が表示されている" do
      click_on product_1.name
      expect(page).to have_content product_1.name
    end

    it "価格が表示されている" do
      click_on product_1.name
      expect(page).to have_content product_1.display_price
    end

    it "説明文が表示されている" do
      click_on product_1.name
      expect(page).to have_content product_1.description
    end

    it "カテゴリーページに戻るリンクがある" do
      click_on product_1.name
      expect(page).to have_link "一覧ページへ戻る", href: potepan_category_path(taxon_1.id)
    end
  end

  context "商品カテゴリーが正しく分けられているか" do
    it "対象カテゴリーの商品が表示されているか" do
      within ".side-nav" do
        click_on taxonomy.name
        click_on "T-Shirts"
      end
      within ".productBox" do
        expect(page).to have_content "rails-T"
      end
    end

    it "別のカテゴリー商品は表示されない" do
      within ".side-nav" do
        click_on taxonomy.name
        click_on "Bags"
      end
      within ".productBox" do
        expect(page).not_to have_content "rails-T"
        expect(page).to have_content "ruby-bag"
      end
    end
  end
end
